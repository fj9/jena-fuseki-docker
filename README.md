# Apache Jena Fuseki


## Unzip Apache Jena Fuseki
```bash
tar -xvf apache-jena-fuseki-3.12.0.tar.gz
```

## Add a statements ttl
Write one yourself. Export out of tool such as GraphDB
```bash
touch statements.ttl
```

## Build the Docker image
```bash
docker build -t jena .
```

## Run the image
```bash
 docker run -p 8080:8080 -it jena
```

## View in browser
localhost:8080
