FROM tomcat:9.0.10


ADD apache-jena-fuseki-3.12.0/fuseki.war /usr/local/tomcat/webapps/
ADD config.ttl /etc/fuseki/
ADD statements.ttl /etc/fuseki/Data/statements.ttl
ADD shiro.ini /etc/fuseki/shiro.ini



